import React from "react";
import ReactDOM from 'react-dom';
import App from "./components/App/App";

jest.mock('react-dom');

describe('Index', () => {
    it('should render App', () => {
        document.getElementById = jest.fn().mockReturnValue('dummyDiv')
        require("./index.js");

        const renderCall = ReactDOM.render.mock.calls[0];
        expect(document.getElementById).toHaveBeenCalledTimes(1);
        expect(document.getElementById).toHaveBeenCalledWith('root');
        expect(renderCall[1]).toEqual('dummyDiv')
        expect(renderCall[0].props.children.type).toEqual(App)
    })
})