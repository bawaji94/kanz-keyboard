import React from "react";
import {shallow} from 'enzyme';
import toJson from 'enzyme-to-json';
import Bismillah from "./Bismillah";

describe('Bismillah', () => {
    it('should render with bismillah font', () => {
        const wrapper = shallow(<Bismillah/>)

        expect(toJson(wrapper)).toMatchSnapshot()
    })
})