import React from 'react';
import App from './App';
import {shallow} from "enzyme";
import toJson from 'enzyme-to-json';
import FontSelector from "../FontSelector/FontSelector";
import TextBox from "../Textbox/TextBox";
import KeyBoard from "../KeyBoard/KeyBoard";

describe('App', () => {
  it('should render 3 components', () => {
    const wrapper = shallow(<App/>);

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should update font if changed', () => {
    const wrapper = shallow(<App/>);

    wrapper.find(FontSelector).props().onChange('dummyFont')

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should update text in Text box is changed', () => {
    const wrapper = shallow(<App/>);

    wrapper.find(TextBox).props().onChange('dummy Text')

    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should update text in Text box when keys in keyboard are clicked', () => {
    const wrapper = shallow(<App/>);

    let mockTextArea = {
      selectionStart: 0,
      selectionEnd: 2,
      focus: jest.fn()
    };
    document.getElementById = jest.fn().mockReturnValue(mockTextArea)

    wrapper.find(KeyBoard).props().onLetterClick('a')
    wrapper.find(KeyBoard).props().onLetterClick('b')

    expect(toJson(wrapper)).toMatchSnapshot()
    expect(mockTextArea.focus).toHaveBeenCalledTimes(2)
  })

  it('should update text in Text box when multiple letters are entered at the same time', () => {
    const wrapper = shallow(<App/>);

    let mockTextArea = {
      selectionStart: 0,
      selectionEnd: 2,
      focus: jest.fn()
    };
    document.getElementById = jest.fn().mockReturnValue(mockTextArea)

    wrapper.find(KeyBoard).props().onLetterClick('a')
    wrapper.find(KeyBoard).props().onLetterClick('bb')
    wrapper.find(KeyBoard).props().onLetterClick('c')

    expect(wrapper.find(TextBox).props().text).toEqual('abbc')
  })
})
