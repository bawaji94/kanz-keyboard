import React, {Component} from 'react';
import './App.css';
import TextBox from "../Textbox/TextBox";
import KeyBoard from "../KeyBoard/KeyBoard";
import FontSelector from "../FontSelector/FontSelector";
import TextBoxText from "../../models/TextBoxText/TextBoxText";
import Footer from "../Footer/Footer";
import Bismillah from "../../Bismillah/Bismillah";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: new TextBoxText(''),
            font: 'Al-Kanz'
        }

        this.addLetter = this.addLetter.bind(this)
        this.updateText = this.updateText.bind(this)
        this.updateFont = this.updateFont.bind(this)
    }

    addLetter(letter) {
        const textBox = document.getElementById('textBox')
        const startIndex = textBox.selectionStart;
        const updatedText = this.state.text.insertText(letter, startIndex, textBox.selectionEnd)
        const afterUpdate = () => {
            textBox.focus();
            textBox.selectionStart = startIndex + letter.length
            textBox.selectionEnd = startIndex + letter.length
        }
        this.updateText(updatedText.text, afterUpdate)
    }

    updateText(updatedText, callBack) {
        this.setState({text: new TextBoxText(updatedText)}, callBack);
    }

    updateFont(font) {
        this.setState({font})
    }

    render() {
        return <div className='App'>
            <Bismillah/>
            <FontSelector value={this.state.font} onChange={this.updateFont}/>
            <TextBox text={this.state.text.text} onChange={this.updateText} font={this.state.font}/>
            <KeyBoard onLetterClick={this.addLetter} font={this.state.font}/>
            <Footer/>
        </div>
    }
}

export default App;
