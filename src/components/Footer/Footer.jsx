import React from "react";
import './Footer.css'

function Footer() {
    return <div className='Footer'>
        The code can be found <a href='https://gitlab.com/bawaji94/kanz-keyboard'>here</a>
    </div>
}

export default Footer