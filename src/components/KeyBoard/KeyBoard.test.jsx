import React from 'react';
import {shallow} from "enzyme";
import toJson from 'enzyme-to-json';
import KeyBoard from "./KeyBoard";
import Key from "../Key/Key";

describe('Key board', () => {
    it('should render all keys', () => {
        const wrapper = shallow(<KeyBoard font='dummyFont' onLetterClick={jest.fn()}/>);

        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('should notify if any of the keys is clicked', () => {
        const dummyOnLetterClick = jest.fn();
        const wrapper = shallow(<KeyBoard font='dummyFont' onLetterClick={dummyOnLetterClick}/>);

        const keyAlif = wrapper.find(Key).findWhere(k => k.props().letter === 'ا')
        keyAlif.props().onLetterClick('ا')

        expect(dummyOnLetterClick).toHaveBeenCalledTimes(1)
        expect(dummyOnLetterClick).toHaveBeenCalledWith('ا')
    })
})
