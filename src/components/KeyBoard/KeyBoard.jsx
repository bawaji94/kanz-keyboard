import React from 'react';
import Key from "../Key/Key";
import './KeyBoard.css'
import {letters} from "../../constants";

function KeyBoard({onLetterClick, font}) {
    return (
        <div className='KeyBoard'>
            <div className='KeyBoard__list'>
                {
                    letters.map((lg, i) =>
                        <div key={i}>
                            {[...lg]
                                .reverse()
                                .map((l, li) => (
                                    <Key
                                        key={li}
                                        letter={l.letter}
                                        onLetterClick={onLetterClick}
                                        font={font}
                                        write={l.write}
                                    />
                                ))}
                        </div>)
                }
            </div>
        </div>
    );
}

export default KeyBoard