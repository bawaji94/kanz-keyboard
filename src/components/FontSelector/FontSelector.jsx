import React from "react";
import './FontSelector.css'
import {fonts} from "../../constants";

function FontSelector({value, onChange}) {
    return <>
        <div className='FontSelector'>
            <span>Choose a font:</span>
            <div className='FontSelector__select'>
                {
                    fonts.map(f => <label key={f.name} className='FontSelector__label'>
                        <input onChange={() => onChange(f.name)} type='radio' value={f.name}
                               checked={value === f.name}/>
                        <a href={f.link} target='_blank' rel="noopener noreferrer">{f.name}</a>
                    </label>)
                }
            </div>
        </div>
        <div className='FontSelector__note'>
            Note: Please download the fonts and install it on your machine to render the arabic letters in the above fonts
            <br/>
            The fonts can also be downloaded from the <a href='http://nujoomapps.com/product/al-kanz-fonts/' target='_blank' rel="noopener noreferrer">Nujoom Apps</a> website, the creators of the font files.
        </div>
    </>
}

export default FontSelector