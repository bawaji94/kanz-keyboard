import React from 'react';
import {shallow} from "enzyme";
import toJson from 'enzyme-to-json';
import FontSelector from "./FontSelector";

describe('Font Selector', () => {
    it('should render a drop down to select font', () => {
        const wrapper = shallow(<FontSelector value='Al-Kanz'/>);

        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('should notify when font is changed', () => {
        const dummyOnChange = jest.fn()
        const wrapper = shallow(<FontSelector value='Al-Kanz' onChange={dummyOnChange}/>);

        wrapper.find('input')
            .findWhere(i => i.props().value === 'Kanz-al-Marjaan')
            .simulate('change', {target: {checked: true}})

        expect(dummyOnChange).toHaveBeenCalledTimes(1)
        expect(dummyOnChange).toHaveBeenCalledWith('Kanz-al-Marjaan')
    })
})