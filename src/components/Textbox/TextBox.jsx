import React from 'react';
import './TextBox.css'

function TextBox({text, onChange, font}) {
    return <div className='TextBox'>
        <textarea
            dir="rtl"
            lang="ar"
            id='textBox'
            spellCheck={false}
            className={`TextBox__textarea ${font}`}
            value={text}
            onChange={(e) => onChange(e.target.value)}
        />
    </div>
}

export default TextBox