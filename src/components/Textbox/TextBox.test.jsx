import React from 'react';
import {shallow} from "enzyme";
import toJson from 'enzyme-to-json';
import TextBox from "./TextBox";

describe('App', () => {
    it('should render 3 components', () => {
        const wrapper = shallow(<TextBox text='dummyText' font='dummyFont'/>);

        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('should notify when directly typed into text box', () => {
        const dummyOnChange = jest.fn()
        const wrapper = shallow(<TextBox text='dummyText' font='dummyFont' onChange={dummyOnChange}/>);

        wrapper.find('textarea').simulate('change', {target: {value: 'changed text'}})

        expect(dummyOnChange).toHaveBeenCalledTimes(1)
        expect(dummyOnChange).toHaveBeenCalledWith('changed text')
    })

    it('should update the text if new text if text is changed', () => {
        const wrapper = shallow(<TextBox text='dummyText' font='dummyFont' onChange={jest.fn()}/>);

        wrapper.setProps({ text: 'changed text' });

        expect(wrapper.find('textarea').props().value).toEqual('changed text')
    })
})
