import React from "react";
import './Key.css'

function Key({letter, onLetterClick, font, write}) {
    return  (
        <button className={`Key ${font}`} onClick={() => onLetterClick(write || letter)}>
            {letter}
        </button>
    )
}

export default Key;