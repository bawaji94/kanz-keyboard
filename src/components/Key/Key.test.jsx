import React from "react";
import { shallow } from 'enzyme';
import Key from "./Key";
import toJson from 'enzyme-to-json';

describe('Key', () => {
    it('should render key with letter', () => {
        const wrapper = shallow(<Key letter='dummyLetter' font='dummyFont'/>);

        expect(toJson(wrapper)).toMatchSnapshot()
    })

    it('should notify when clicked', () => {
        const dummyOnChange = jest.fn()
        const wrapper = shallow(<Key letter='dummyLetter' font='dummyFont' onLetterClick={dummyOnChange}/>);

        wrapper.find('button').simulate('click')

        expect(dummyOnChange).toHaveBeenCalledTimes(1)
        expect(dummyOnChange).toHaveBeenCalledWith('dummyLetter')
    })

    it('should notify with write when provided', () => {
        const dummyOnChange = jest.fn()
        const wrapper = shallow(
            <Key letter='dummyLetter' font='dummyFont' onLetterClick={dummyOnChange} write='dummyOtherLetter'/>
        );

        wrapper.find('button').simulate('click')

        expect(dummyOnChange).toHaveBeenCalledTimes(1)
        expect(dummyOnChange).toHaveBeenCalledWith('dummyOtherLetter')
    })
})