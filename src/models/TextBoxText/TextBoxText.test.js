import TextBoxText from "./TextBoxText";

describe('Text box text', () => {
    it('should add text towards the end of the string', () => {
        const textBoxText = new TextBoxText('اب');

        const updatedTextBoxText = textBoxText.insertText('ت', 2, 2)

        expect(updatedTextBoxText.text).toEqual('ابت')
    });

    it('should add text towards the start of the string', () => {
        const textBoxText = new TextBoxText('اب');

        const updatedTextBoxText = textBoxText.insertText('ت', 0, 0)

        expect(updatedTextBoxText.text).toEqual('تاب')
    });

    it('should add text in the middle', () => {
        const textBoxText = new TextBoxText('اب');

        const updatedTextBoxText = textBoxText.insertText('ت', 1, 1)

        expect(updatedTextBoxText.text).toEqual('اتب')
    });

    it('should replace the highlighted text', () => {
        const textBoxText = new TextBoxText('ابببب');

        const updatedTextBoxText = textBoxText.insertText('ت', 1, 4)

        expect(updatedTextBoxText.text).toEqual('اتب')
    });
})