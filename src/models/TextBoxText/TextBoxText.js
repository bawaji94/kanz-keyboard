class TextBoxText {
    constructor(text) {
        this.text = text
    }

    insertText(textEntered, startIndex, endIndex) {
        const start = this.text.substring(0, startIndex)
        const end = this.text.substring(endIndex, this.text.length)
        return new TextBoxText(start + textEntered + end);
    }
}

export default TextBoxText