export const letters = [
    [
        {
            "letter": "ا"
        },
        {
            "letter": "ب"
        },
        {
            "letter": "ت"
        },
        {
            "letter": "ث"
        },
        {
            "letter": "پ"
        },
        {
            "letter": "ج"
        },
        {
            "letter": "ح"
        },
        {
            "letter": "خ"
        },
        {
            "letter": "چ"
        },
        {
            "letter": "د"
        },
        {
            "letter": "ذ"
        },
    ],
    [
        {
            "letter": "ر"
        },
        {
            "letter": "ز"
        },
        {
            "letter": "س"
        },
        {
            "letter": "ش"
        },
        {
            "letter": "ص"
        },
        {
            "letter": "ض"
        },
        {
            "letter": "ط"
        },
        {
            "letter": "ظ"
        },
        {
            "letter": "ع"
        },
        {
            "letter": "غ"
        },
        {
            "letter": "ف"
        },
        {
            "letter": "ق"
        },
    ],
    [
        {
            "letter": "ک"
        },
        {
            "letter": "گ"
        },
        {
            "letter": "ل"
        },
        {
            "letter": "م"
        },
        {
            "letter": "ن"
        },
        {
            "letter": "طط"
        },
        {
            "letter": "و"
        },
        {
            "letter": "ؤ"
        },
        {
            'letter': "ـظظ",
            'write': "ظظ"
        },
        {
            "letter": "ه",
        },
        {
            "letter": "ة"
        }
    ],
    [
        {
            "letter": "ي"
        },
        {
            "letter": "ی"
        },
        {
            "letter": "ئ"
        },
        {
            "letter": "ء"
        },
        {
            "letter": "سس"
        },
        {
            "letter": "ڈ"
        },
        {
            "letter": "ڑ"
        },
        {
            "letter": "؛"
        }
    ],
    [
        {
            "letter": "ٰ"
        },
        {
            "letter": "ٖ"
        },
        {
            "letter": "ٗ"
        },
        {
            "letter": "َ"
        },
        {
            "letter": "ِ"
        },
        {
            "letter": "ُ"
        },
        {
            "letter": "ً"
        },
        {
            "letter": "ٍ"
        },
        {
            "letter": "ٌ"
        },
        {
            "letter": "ّ"
        },
        {
            "letter": "ـ"
        },
        {
            "letter": "؟"
        },
        {
            "letter": "،"
        },
    ],
    [
        {
            "letter": "٠"
        },
        {
            "letter": "١"
        },
        {
            "letter": "٢"
        },
        {
            "letter": "٣"
        },
        {
            "letter": "٤"
        },
        {
            "letter": "٥"
        },
        {
            "letter": "٦"
        },
        {
            "letter": "٧"
        },
        {
            "letter": "٨"
        },
        {
            "letter": "٩"
        }
    ]
]

export const fonts = [
    {
        name: 'Al-Kanz',
        link: 'https://my.syncplicity.com/share/oeiquvsxrvdm7ls/Al-Kanz%20fonts'
    },
    {
        name: 'Kanz-al-Marjaan',
        link: 'https://my.syncplicity.com/share/6uk1jz6dtmt8s57/Kanz-al-Marjaan'
    }
]