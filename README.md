# Description

This app is created to render the arabic letter using Al-Kanz and Kanz-al-Marjaan fonts created by [Nujoom Apps](http://nujoomapps.com/)

## Start the app
`yarn start` or `npm run start`

## Run tests
`yarn test` or `npm run test` 

#Demo
The demo of the keyboard can be found [here](https://kanz-keyboard.firebaseapp.com/)